1. Install node with npm https://nodejs.org
2. Install packages: npm i
3. To run dev mode use command: npm run dev
4. To build project for test server: npm run prod-test
5. To build project for production server:
		a) in config APP set: const BUILD_MOD = 'prod'
		b) npm run prod

Note!
1. To config APP use:
	./src/react/__config/index.js
2. To configuration webpack, use:
	./webpack-config.js
	./config/webpack.config.dev.js
	./config/webpack.config.prod-test.js
