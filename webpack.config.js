const path = require('path')
const production = require('./config/webpack.config.prod.js')
const development = require('./config/webpack.config.dev.js')

const NODE_ENV = process.env.NODE_ENV || 'prod'

if (NODE_ENV === 'dev') {
  module.exports = development({
    entry: {
      index: './src/react/index.jsx',
    },
    filename: 'js/[name].js',
    publicPath: 'http://localhost:3000/',
    outputPath: path.join(__dirname, '/public'),
    devServerPort: 3000,
    resolvePath: ['src/react'],
  })
}


if (NODE_ENV === 'prodTest' || NODE_ENV === 'prod') {
  module.exports = production({
    entry: {
      index: './src/react/index.jsx',
    },
    filename: 'js/[name].js',
    publicPath: (NODE_ENV === 'prodTest')
      ? 'http://laysvoting.ddaproduction.com'
      : 'https://lays.ua',
    outputPath: path.join(__dirname, '/public'),
    resolvePath: ['src/react'],
  })
}
