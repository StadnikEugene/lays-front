$(document).ready(function() {

	/* fullpage */
	var loaded = false;
	function createFullpage() {
		if(fullPageCreated === false) {
			fullPageCreated = true;
			new fullpage('#fullpage', {
				licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
				verticalCentered: false,
				css3: false,
				scrollingSpeed: 700,
				easingcss3: 'ease',
				scrollOverflow: false,
				onLeave: function(origin, destination, direction){
		            if(destination.index == 0){
		                history.replaceState(null, "page 1", "/main");
		            }
		            if(destination.index == 1){
		                history.replaceState(null, "page 2", "/gadgets");
		            }
		            if(destination.index == 2){
		                history.replaceState(null, "page 3", "/flavors");
		            }
		        },
				afterRender: function(anchorLink, index){
		            if(!loaded){
		                if(window.location.href.indexOf("main") > -1) {
		                    fullpage_api.silentMoveTo(1, 0);
		                }
		                if(window.location.href.indexOf("gadgets") > -1) {
		                    fullpage_api.silentMoveTo(2, 0);
		                }
		                if(window.location.href.indexOf("flavors") > -1) {
		                    fullpage_api.silentMoveTo(3, 0);
		                }
		                loaded = true;
		            }
		        }
			});
		}
	}
	var fullPageCreated = false;

	if(document.documentElement.clientWidth > 1023 && document.documentElement.clientHeight > 624) {
		createFullpage();
	}
	$(window).on('resize orientationChange', function() {
		if ( $(window).width() > 1023 && $(window).height() > 624 ) {
			createFullpage();
		} else {
			if(fullPageCreated == true) {
				fullPageCreated = false;
				fullpage_api.destroy('all');
			}
		}
	});

	$(".next-block").on("click" , function(){
       fullpage_api.moveSectionDown();
       return false;
    });

	/* stages */
	$('.stages').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		arrows: false,
		dots: false,
		draggable: true,
		fade: false,
		speed: 1000,
		variableWidth: true,
		responsive: [
		    {
		      breakpoint: 1220,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        variableWidth: false,
		      }
		    },
		]
	});

	fetch('https://lays.ua/api/v1/voting/weeks')
		.then((response) => {
			return response.json()
		})
		.then((result) => {
			const week = result.data
			/* prizes */
			$('.prizes-carousel').slick({
				infinite: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				autoplay: false,
				arrows: true,
				dots: false,
				draggable: true,
				initialSlide: Number(week) - 2,
				fade: false,
				speed: 1000,
				centerMode: true,
				centerPadding: "0px",
				responsive: [
				    {
				      breakpoint: 768,
				      settings: {
				        slidesToShow: 1,
				      }
				    },
				],
			});
		})
		.catch((e) => {
			console.log(e)
		})

	// /* click button */
	// $('.btn-vote').on('click', function(){
	// 	var el = $(this).prev('.pack-img').find('img');
	// 	el.removeClass('bounce');
	// 	setTimeout(function(){
	//        el.addClass('bounce');
	//     },20);
	// });

	/**/
	$(".scroll-box").mCustomScrollbar({});

});

/* Parallax */
function Parallax(options){
    options = options || {};
    this.nameSpaces = {
        wrapper: options.wrapper || '.parallax',
        layers: options.layers || '.parallax-layer',
        deep: options.deep || 'data-parallax-deep'
    };
    this.init = function() {
        var self = this,
            parallaxWrappers = document.querySelectorAll(this.nameSpaces.wrapper);
      	for(var i = 0; i < parallaxWrappers.length; i++){
			(function(i){
				parallaxWrappers[i].addEventListener('mousemove', function(e){
					var x = e.clientX,
						y = e.clientY,
						layers = parallaxWrappers[i].querySelectorAll(self.nameSpaces.layers);
					for(var j = 0; j < layers.length; j++){
            (function(j){
              var deep = layers[j].getAttribute(self.nameSpaces.deep),
                  disallow = layers[j].getAttribute('data-parallax-disallow'),
                  itemX = (disallow && disallow === 'x') ? 0 : x / deep,
                  itemY = (disallow && disallow === 'y') ? 0 : y / deep;
                  if(disallow && disallow === 'both') return;
                  layers[j].style.transform = 'translateX(' + itemX + '%) translateY(' + itemY + '%)';
            })(j);
					}
				})
			})(i);
      	}
    };
    this.init();
    return this;
}

window.addEventListener('load', function(){
    new Parallax();
});

if(document.documentElement.clientWidth < 1024 && document.documentElement.clientHeight < 625) {
	/* iphone 100vh */
	var vh = window.innerHeight * 0.01;
	document.getElementById('firstScreen').style.setProperty('--vh', vh + 'px');
	document.getElementById('firstScreenT').style.setProperty('--vh', vh + 'px');
}
