document.addEventListener('DOMContentLoaded', function () {


    var mobdevwith = 640;
    var descdevwith = 1024;
    var windowOrientation = getOrientation();
    var viewport = document.getElementById('viewport');
    setViewport();
    window.addEventListener('resize', function () {
        if (!window.screen || window.screen.width < 768) {
            var a = getOrientation();
            if (a !== windowOrientation) {
                windowOrientation = a;
                setViewport()
            }
        }
    });

    function getOrientation() {
        var a = window.innerWidth / window.innerHeight;
        return a > 1 ? 'landscape' : 'portrait'
    }

    function setViewport() {
        setTimeout(function () {
            viewport.setAttribute('content', 'width=device-width ,initial-scale=1.0, user-scalable=0');
            if (window.innerWidth <= mobdevwith) {
                var a = window.innerWidth / mobdevwith;
                viewport.setAttribute('content', 'width=' + mobdevwith + ', initial-scale=' + a + ', user-scalable=0')
            }
            if (-1 < navigator.userAgent.indexOf('Macintosh')) {
                viewport.setAttribute('content', 'width=' + descdevwith)
            }
        }, 0);
    }



    const menu = document.querySelector('.menu'),
        menuBtn = document.querySelector('.menu__toggler');
    menuBtn.onclick = () => {
        menu.classList.toggle('opened');
        menuBtn.classList.toggle('opened');
    }

    let holder_1 = document.querySelector('#holder-1'),
        holder_2 = document.querySelector('#holder-2'),
        sec      = document.querySelector('.meet');

    const changeKus = () => {
        if(holder_1.classList.contains('active')){
            holder_1.classList.remove('active')
            holder_2.classList.add('active')
            sec.classList.remove('green')
            sec.classList.add('red')

        }else if (holder_2.classList.contains('active')){
            holder_2.classList.remove('active')
            holder_1.classList.add('active')
            sec.classList.remove('red')
            sec.classList.add('green')
        }
    }


    setInterval(changeKus, 5000)


    document.querySelectorAll('.m-h__img-holder').forEach(elem => {
        elem.addEventListener('click', function (e){
            this.classList.toggle('active')
        })
    })


})
