
import {
  required,
  length,
  format,
  email,
  acceptance,
} from 'redux-form-validators'
import messages from './messages'

export const nameValidate = [
  required({ msg: messages.requiredField }),
  format({
    with: /^[A-Z,А-Я,a-z,а-я,ё,Ё,\s,а-я,А-Я,Ї,ї,Є,є,І,і,ґ,Ґ]+$/i,
    message: messages.wrongName,
  }),
]

export const phoneValidate = [
  required({ msg: messages.requiredField }),
  format({
    with: /^\+38 \([0-9]{3}\) [0-9]{3} [0-9]{2} [0-9]{2}$/,
    message: messages.wrongPhone,
  }),
]

export const passwordValidate = [
  required({ msg: messages.requiredField }),
  length({
    msg: { tooShort: messages.shortPassword, tooLong: messages.longPassword },
    in: [6, 100],
  }),
]

export const emailValidate = [
  required({ msg: messages.requiredField }),
  email({ msg: messages.wrongEmail }),
]

export const checkboxValidate = [
  acceptance({ msg: messages.ruleConfirm }),
]

export const dateValidate = [
  required({ msg: messages.requiredField }),
]
