import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { changeModal, setCloseModalHendler } from 'redux/modal/actions'
import Component from '_common/components/Modal'
import scrollPageApi from 'utils/scroll-page-api'

class Modal extends React.Component {
  state = {
    show: false,
  }

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.element),
      PropTypes.element,
    ]).isRequired,
    changeModal: PropTypes.func.isRequired,
    closeModalHendler: PropTypes.func.isRequired,
    setCloseModalHendler: PropTypes.func.isRequired,
    modalCloseRedirect: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
    ]),
  }

  static defaultProps = {
    modalCloseRedirect: false,
  }

  componentDidMount() {
    const {
      setCloseModalHendler: setCloseHendlerAction,
    } = this.props

    scrollPageApi.disable()
    setCloseHendlerAction(this.onCloseHendler)

    setTimeout(() => {
      this.setState({
        show: true,
      })
    }, 10)
  }

  onCloseHendler = () => {
    const {
      changeModal: changeModalAction,
      modalCloseRedirect: modalCloseRedirectAction,
    } = this.props

    scrollPageApi.enable()

    if (modalCloseRedirectAction) {
      changeModalAction({ modalName: modalCloseRedirectAction })
      return
    }
    changeModalAction({ modalName: false })
  }

  render() {
    const { children, closeModalHendler: closeModal } = this.props
    const { show } = this.state

    return (
      <Component
        closeModal={closeModal}
        show={show}
      >
        {children}
      </Component>
    )
  }
}


const MSTP = state => ({
  closeModalHendler: state.modal.closeModalHendler,
  modalCloseRedirect: state.modal.modalCloseRedirect,
})

const MDTP = {
  changeModal,
  setCloseModalHendler,
}

export default connect(MSTP, MDTP)(Modal)
