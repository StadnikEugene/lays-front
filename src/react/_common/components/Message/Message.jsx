import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'

import Modal from '_common/containers/Modal'

import './style.scss'

const Message = (props) => {
  const {
    closeModalHendler,
    messageStore,
    message,
    children,
  } = props

  const msg = message || messageStore

  const getButtonClose = () => (
    message.buttonClose
      ? <button className="btn btn-style-2" onClick={closeModalHendler} type="button">{message.buttonClose}</button>
      : false
  )

  return (
    <Modal>
      <div className="modal-head message">
        <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={closeModalHendler}
        />
        <div className="logo-wrap">
          <span className="logo">
            <img src="./images/logo-sm.png" alt="Чумак" />
          </span>
        </div>
        <p className={cn('h3', { 'not-success': msg.status === 'notSuccess' })}>
          {msg.title}
        </p>
      </div>
      <div className="modal-body">
        <div className="message">
          <p>
            {msg.message}
          </p>
          <p>
            {msg.message2}
          </p>
        </div>
        {children}
        { getButtonClose() }
      </div>
    </Modal>
  )
}

Message.propTypes = {
  closeModalHendler: PropTypes.func.isRequired,
  message: PropTypes.objectOf(PropTypes.string).isRequired,
  messageStore: PropTypes.objectOf(PropTypes.string).isRequired,
}

const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
  messageStore: state.message.message,
})


export default connect(mapStateToProps)(Message)
