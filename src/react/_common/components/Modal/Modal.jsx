import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

const Modal = ({ closeModal, children, show }) => (
  <div
    className={cn('Login modal fade', { show })}
    onClick={closeModal}
    id="modal"
    tabIndex="-1"
    role="dialog"
    style={{ paddingRight: '0px', display: 'block' }}
  >
    <div
      className="modal-dialog"
      role="document"
    >
      <div
        className="modal-content"
        onClick={(e) => {e.stopPropagation()}}
      >
        {children}
      </div>
    </div>
  </div>
)

Modal.propTypes = {
  closeModal: PropTypes.func.isRequired,
}

export default Modal
