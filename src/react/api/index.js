import axios from 'axios'
import { BASE_API_PATH } from '__config'

// refresh token

export const apiRefresh = ({ refreshToken }) => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/user/refresh`, {}, { headers: { Authorization: refreshToken } })
      .then((response) => {
        const { access_token: token } = response.data.data
        resolve(token)
      })
      .catch((e) => {
        console.warn(e)
        reject()
      })
  })
)

// usreInfo

export const apiUserInfo = ({ token }) => (
  new Promise((resolve, reject) => {
    axios.get(`${BASE_API_PATH}/user/userinfo`, { headers: { Authorization: token } })
      .then((result) => {
        resolve(result)
      })
      .catch((err) => {
        if (err.response) {
          const { data } = err.response.data
          if (data === 'access token expired') {
            reject(data)
            return
          }
        }

        reject({})
      })
  })
)

// login

export const apiLogin = data => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/user/login`, data)
      .then((result) => {
        const { data: userData } = result.data

        resolve({
          userData: userData.user,
          token: userData.tokens[0].access_token,
          refreshToken: userData.tokens[0].refresh_token,
        })
      })
      .catch((err) => {
        if (err.response && err.response.data.status === 'failure') {
          reject(err.response.data.status)
          return
        }

        reject()
      })
  })
)

// registration

export const apiRegistration = data => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/user/registration`, data)
      .then((result) => {
        const { data: userData } = result.data
        resolve({
          userData: userData.user,
          token: userData.tokens[0].access_token,
          refreshToken: userData.tokens[0].refresh_token,
        })
      })
      .catch((err) => {
        if (!err.response) {
          reject({})
          return
        }

        if (err.response.status === 409) {
          reject({
            code: 'phone',
            message: err.response.data.data,
          })
          return
        }

        if (err.response.status === 401) {
          reject({
            code: 'email',
            message: err.response.data.data,
          })
          return
        }

        reject({})
      })
  })
)

// getRules

export const apiGetRules = () => (
  new Promise((resolve, reject) => {
    axios.get(`${BASE_API_PATH}/info/rules`)
      .then((result) => {
        const { content } = result.data.data
        resolve(content)
      })
      .catch(() => {
        reject()
      })
  })
)


// recoveryPassword

export const apiPasswordRecovery = email => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/user/forgot`, { email })
      .then((result) => {
        resolve({
          message: result.data.status,
        })
      })
      .catch((err) => {
        if (!err.response) {
          reject({})
          return
        }
        if (err.response.status === 404) {
          reject({
            message: err.response.data.data,
          })
          return
        }
        reject({})
      })
  })
)


// getWinners

export const apiGetWinners = () => (
  new Promise((resolve, reject) => {
    axios.get(`${BASE_API_PATH}/info/winners`)
      .then((result) => {
        resolve(result.data.data)
      })
      .catch(() => {
        reject()
      })
  })
)

// getWeek

export const apiGetWeek = () => (
  new Promise((resolve, reject) => {
    axios.get(`${BASE_API_PATH}/voting/weeks`)
      .then((result) => {
        resolve(result.data.data)
      })
      .catch(() => {
        reject()
      })
  })
)

// getVotes

export const apiGetVotes = () => (
  new Promise((resolve, reject) => {
    axios.get(`${BASE_API_PATH}/info/votes`)
      .then((result) => {
        resolve(result.data.data)
      })
      .catch(() => {
        reject()
      })
  })
)

// sendVote

export const apiSendVote = ({ token, data }) => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/voting/vote`,
      data,
      { headers: { Authorization: token } })
      .then((result) => {
        resolve({
          message: result.data.data,
        })
      })
      .catch((err) => {
        if (!err.response) {
          reject({})
          return
        }

        if (err.response.status === 400 || err.response.status === 403) {
          reject({
            message: err.response.data.data,
          })
        }
      })
  })
)

// sendFacebookID

export const apiSendFacebookData = ({ token, data }) => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/voting/addfbinfo`,
      data,
      { headers: { Authorization: token } })
      .then((result) => {
        resolve({
          message: result.data.status,
        })
      })
      .catch((err) => {
        if (!err.response) {
          reject({})
          return
        }

        if (err.response.status === 400 || err.response.status === 403) {
          reject({
            message: err.response.data.data,
          })
        }
      })
  })
)

// sendFacebookShare

export const apiSendFacebookShare = ({ token, data }) => (
  new Promise((resolve, reject) => {
    axios.post(`${BASE_API_PATH}/voting/share`,
      data,
      { headers: { Authorization: token } })
      .then((result) => {
        resolve({
          message: result.data.status,
        })
      })
      .catch((err) => {
        if (!err.response) {
          reject({})
          return
        }

        if (err.response.status === 400 || err.response.status === 403) {
          reject({
            message: err.response.data.status,
          })
        }
      })
  })
)

// checkWin

export const apiCheckWin = ({ token }) => (
  new Promise((resolve, reject) => {
    axios.get(`${BASE_API_PATH}/voting/checkwin`, { headers: { Authorization: token } })
      .then((result) => {
        const { data } = result.data
        resolve(data)
      })
      .catch((err) => {
        if (!err.response) {
          reject({})
          return
        }

        if (err.response.status === 400) {
          reject({
            message: err.response.data.status,
          })
        }
      })
  })
)
