import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import './style.scss'

// Component

const TipValid = (props) => {
  const { error, nowrap } = props

  return (
    <div
      className={cn('tip-valid', { 'tip-valid__nowrap': nowrap })}
    >
      {error}
    </div>
  )
}

TipValid.propTypes = {
  error: PropTypes.string.isRequired,
  nowrap: PropTypes.bool,
}

TipValid.defaultProps = {
  nowrap: null,
}


export default TipValid
