import React from 'react'
import PropTypes from 'prop-types'
import ReactDom from 'react-dom'

const Votes = ({ onClick, voted }) => {
  const voteContainer1 = document.querySelector('.btn-vote-container-1')
  const voteContainer2 = document.querySelector('.btn-vote-container-2')

  const getButtonVote1 = () => (
    <button
      className="btn-vote"
      onClick={onClick}
      type="button"
      id='vote-nk'
      data-click-id='vote-nk'
      data-type="1"
    >
      { voted === 1 ? 'ОБРАНИЙ ТОБОЮ СМАК' : 'Голосуй за смак!' }
    </button>
  )

  const getButtonVote2 = () => (
    <button
      className="btn-vote"
      onClick={onClick}
      type="button"
      data-click-id='vote-dz'
      id='vote-dz'
      data-type="2"
    >
      { voted === 2 ? 'ОБРАНИЙ ТОБОЮ СМАК' : 'Голосуй за смак!' }
    </button>
  )

  return (
    <React.Fragment>
      { ReactDom.createPortal(getButtonVote1(), voteContainer1) }
      { ReactDom.createPortal(getButtonVote2(), voteContainer2) }
    </React.Fragment>
  )
}


Votes.propTypes = {
  onClick: PropTypes.func.isRequired,
}


export default Votes
