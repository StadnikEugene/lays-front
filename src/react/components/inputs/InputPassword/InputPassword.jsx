import React from 'react'
import TipValid from 'components/TipValid'
import cn from 'classnames'

// Component

const InputPassword = ({ input, meta, ...props }) => {
  const { active, touched, invalid } = meta

  const isNotValid = !active && touched && invalid

  return (
    <div className={cn('input-password', { 'is-valid': !invalid })}>
      <input
        {...input}
        {...props}
      />
      { isNotValid && <TipValid error={meta.error} nowrap /> }
    </div>
  )
}

export default InputPassword
