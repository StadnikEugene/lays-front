import React from 'react'
import TipValid from 'components/TipValid'
import InputMask from 'react-input-mask'
import cn from 'classnames'

// Component

const InputPhone = ({ input, meta, ...props }) => {
  const { active, touched, invalid } = meta

  const isNotValid = !active && touched && invalid

  return (
    <div className={cn('input-phone', { 'is-valid': !invalid })}>
      <InputMask
        {...input}
        {...props}
        mask="+38 (999) 999 99 99"
        maskChar=" "
      />
      { isNotValid && <TipValid error={meta.error} nowrap /> }
    </div>
  )
}

export default InputPhone
