import React from 'react'
import TipValid from 'components/TipValid'

// Component

const InputCheckboxTerms = ({ htmlFor, input, meta, ...props }) => {
  const { touched, invalid } = meta

  const isNotValid = touched && invalid

  return (
    <React.Fragment>
      <input
        {...input}
        {...props}
      />
      <label
        htmlFor={htmlFor}
      />
      { isNotValid && <TipValid error={meta.error} nowrap /> }
    </React.Fragment>
  )
}

export default (InputCheckboxTerms)
