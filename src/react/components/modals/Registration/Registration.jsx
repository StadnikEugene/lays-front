import React from 'react'
import PropTypes from 'prop-types'
import Modal from '_common/containers/Modal'
import FormRegistration from 'containers/forms/FormRegistration'

// Component

const Registration = ({ closeModal }) => (
  <Modal>
    <div className="modal-head">
      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal} />
      <div className="logo">
        <img src="./images/logo-sm.png" alt="lays" />
      </div>
      <div className="logo-mob">
        <img src="./images/logo-mob-sm.png" alt="lays" />
      </div>
      <p className="h3">Реєстрація</p>
    </div>
    <div className="modal-body">
      <FormRegistration />
    </div>
  </Modal>
)


Registration.propTypes = {
  closeModal: PropTypes.func.isRequired,
}

export default Registration
