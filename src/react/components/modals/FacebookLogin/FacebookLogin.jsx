import React from 'react'

import Message from '_common/components/Message'
import FBLogin from 'containers/FBLogin'
import messages from './messages'

// Component

const FacebookLogin = () => (
  <Message message={messages}>
    <FBLogin />
  </Message>
)

export default FacebookLogin
