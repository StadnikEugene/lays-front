import React from 'react'
import PropTypes from 'prop-types'

import Modal from '_common/containers/Modal'
import FormLogin from 'containers/forms/FormLogin'

// Component

const Login = ({ closeModal }) => (
  <Modal>
    <div className="modal-head">
      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal} />
      <div className="logo">
        <img src="./images/logo-sm.png" alt="lays" />
      </div>
      <div className="logo-mob">
        <img src="./images/logo-mob-sm.png" alt="lays" />
      </div>
      <p className="h3">Вхід</p>
    </div>
    <div className="modal-body">
      <div className="message">
        <p>
          <span className="warning">
            Увага!
          </span>
            Щоб твій голос було зараховано і ти мав змогу взяти участь у розіграшах призів, будь ласка, авторизуйся або зареєструйся.
        </p>
      </div>
      <FormLogin />
    </div>
  </Modal>
)


Login.propTypes = {
  closeModal: PropTypes.func.isRequired,
}

export default Login
