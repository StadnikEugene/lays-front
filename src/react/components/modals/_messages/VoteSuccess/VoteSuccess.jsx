import React from 'react'

import Message from '_common/components/Message'
import FBLogin from 'containers/FBLogin'
import messages from './messages'

// Component

const VoteSuccess = () => (
  <Message message={messages}>
    <FBLogin />
  </Message>
)

export default VoteSuccess
