
export default {
  title: 'УВАГА!',
  message: 'Вибач, ти можеш поділитися зіркосмаком тільки один раз.',
  buttonClose: 'ЗРОЗУМІЛО',
}
