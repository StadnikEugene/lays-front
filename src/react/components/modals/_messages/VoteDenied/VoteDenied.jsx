import React from 'react'

import Message from '_common/components/Message'
import messages from './messages'

// Component

const VoteDenied = () => (
  <Message message={messages} />
)

export default VoteDenied
