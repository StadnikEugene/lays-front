import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Message from '_common/components/Message'
import { changeModal } from 'redux/modal/actions'
import scrollPageApi from 'utils/scroll-page-api'
import messages from './messages'

// Component

const ShareSuccess = ({ changeModal: changeModalAction, modal }) => {
  useEffect(() => {
    setTimeout(() => {
      if (modal === 'shareSuccess') {
        scrollPageApi.enable()
        changeModalAction({ modalName: false })
      }
    }, 3000)
  })

  return (
    <Message message={messages} />
  )
}

ShareSuccess.propTypes = {
  changeModal: PropTypes.func.isRequired,
  modal: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]).isRequired,
}

const mapStateToProps = state => ({
  modal: state.modal.modalName,
})

const mapDispatchToProps = {
  changeModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShareSuccess)
