import React from 'react'

import Message from '_common/components/Message'
import messages from './messages'

// Component

const RecoverySuccess = () => (
  <Message message={messages} />
)

export default RecoverySuccess
