import React from 'react'
import PropTypes from 'prop-types'
import CustomScroll from 'react-scrollbar'

import Modal from '_common/containers/Modal'
import parseDate from 'utils/parse-date-from-server'
import messages from './messages'

import './style.scss'


// Component

const CheckWin = (props) => {
  const { closeModalHendler, win: myWin } = props

  const getList = win => (
    win.map(item => (
      <tr key={item.id}>
        <td>{parseDate(item.activatetime)}</td>
        <td className="right">{messages[item.type]}</td>
      </tr>
    ))
  )

  return (
    <Modal>
      <div className="modal-head">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModalHendler} />
        <div className="logo">
          <img src="./images/logo-sm.png" alt="lays" />
        </div>
        <div className="logo-mob">
          <img src="./images/logo-mob-sm.png" alt="lays" />
        </div>
        <p className="h5">Твій виграш:</p>
      </div>
      <div className="modal-body">
        <div className="results">
          <div className="tab-content">
            <div className="tab-pane fade show active" id="tab-1" role="tabpanel">
              <div className="scroll-box">
                <CustomScroll
                  className="body"
                  stopScrollPropagation
                >
                  <table>
                    <tbody>
                      {getList(myWin)}
                    </tbody>
                  </table>
                </CustomScroll>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

CheckWin.propTypes = {
  closeModalHendler: PropTypes.func.isRequired,
  win: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default CheckWin
