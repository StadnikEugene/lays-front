import React from 'react'
import PropTypes from 'prop-types'
import CustomScroll from 'react-scrollbar'

import Modal from '_common/containers/Modal'
import parseDate from 'utils/parse-date-from-server'

import './style.scss'


// Component

const Winners = (props) => {
  const { closeModalHendler, winners: allWinners } = props


  const checkWin = (type) => {
    switch (type) {
      case 1:
        return 'Компактні навушники'
      case 2:
        return 'Компактні навушники'
      case 3:
        return 'Ігрова консоль'
      case 4:
        return 'Хітові навушники'
      case 5:
        return 'Акустична система'
      case 6:
        return 'Смарт-годинник'
      case 7:
        return '"Яблучний" планшет'
      case 8:
        return '"Яблучний" смартфон'
      case 9:
        return '"Яблучний" ноутбук'
      default:
        ''

    }
  }

  const getList = winners => (
    winners.map(item => (
      <tr key={item.id}>
        <td>{parseDate(item.activatetime)}</td>
        <td>{item.name}</td>
        <td>{
          checkWin(item.type)
        }</td>
      </tr>
    ))
  )



  return (
    <Modal>
      <div className="modal-head">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModalHendler} />
        <div className="logo">
          <img src="./images/logo-sm.png" alt="lays" />
        </div>
        <div className="logo-mob">
          <img src="./images/logo-mob-sm.png" alt="lays" />
        </div>
        <p className="h5">Результати щоденних розіграшів місячного запасу Lay’s, а також щотижневих розіграшів зіркових гаджетів.</p>
      </div>
      <div className="modal-body">
        <div className="results">
          <div className="nav" role="tablist">
            <a className="active" data-toggle="tab" href="#tab-1" role="tab" aria-selected="true">МІСЯЧНИЙ ЗАПАС LAY’S</a>
            <a data-toggle="tab" href="#tab-2" role="tab" aria-selected="false">ЗІРКОГАДЖЕТИ</a>
          </div>
          <div className="tab-content">
            <div className="tab-pane fade show active" id="tab-1" role="tabpanel">
              <div className="scroll-box">
                <CustomScroll
                  className="body"
                  stopScrollPropagation
                >
                  <table>
                    <tbody>
                      {getList(allWinners.days)}
                    </tbody>
                  </table>
                </CustomScroll>
              </div>
            </div>
            <div className="tab-pane fade" id="tab-2" role="tabpanel">
              <div className="scroll-box">
                <CustomScroll
                  className="body"
                  stopScrollPropagation
                >
                  <table className='win-table'>
                    <tbody>
                    <tr>
                      <th>Дата</th>
                      <th>Ім'я</th>
                      <th>Гаджет</th>
                    </tr>
                      {getList(allWinners.weeks)}
                    </tbody>
                  </table>
                </CustomScroll>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

Winners.propTypes = {
  closeModalHendler: PropTypes.func.isRequired,
  winners: PropTypes.objectOf(PropTypes.array).isRequired,
}

export default Winners
