import React from 'react'
import PropTypes from 'prop-types'

import Modal from '_common/containers/Modal'
import CustomScroll from 'react-scrollbar'
import ReactHtmlParser from 'react-html-parser'

import './style.scss'

// Component

const Rules = ({ closeModal, rules }) => (
  <Modal>
    <div className="modal-head">
      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal} />
      <div className="logo-wrap">
        <span className="logo">
          <img src="./images/logo-sm.png" alt="lays" />
        </span>
      </div>
      <p className="h3">Офіційні правила</p>
    </div>
    <div className="modal-body">
      <div className="official-rules">
        <div className="text-center">
          <p>Офіційні правила (надалі – «Офіційні правила») Акції «Обери свій зіркосмак» (надалі – Акція)</p>
        </div>
        <CustomScroll
          className="scroll"
          stopScrollPropagation
        >
          {ReactHtmlParser(rules)}
        </CustomScroll>
      </div>
    </div>
  </Modal>
)


Rules.propTypes = {
  closeModal: PropTypes.func.isRequired,
  rules: PropTypes.string.isRequired,
}

export default Rules
