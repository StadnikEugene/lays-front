import React from 'react'
import PropTypes from 'prop-types'
import Modal from '_common/containers/Modal'
import FormRecovery from 'containers/forms/FormRecovery'
// import FormRegistration from 'containers/forms/FormRegistration'

// Component

const PasswordRecovery = ({ closeModal }) => (
  <Modal>
    <div className="modal-head">
      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal} />
      <div className="logo-wrap">
        <span className="logo">
          <img src="./images/logo-sm.png" alt="lays" />
        </span>
      </div>
      <p className="h3">Вiдновити пароль</p>
    </div>
    <div className="modal-body">
      <FormRecovery />
    </div>
  </Modal>
)


PasswordRecovery.propTypes = {
  closeModal: PropTypes.func.isRequired,
}

export default PasswordRecovery
