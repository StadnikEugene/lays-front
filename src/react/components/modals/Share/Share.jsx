import React from 'react'

import Message from '_common/components/Message'
import FBShare from 'containers/FBShare'
import messages from './messages'

// Component

const Share = () => (
  <Message message={messages}>
    <FBShare />
  </Message>
)

export default Share
