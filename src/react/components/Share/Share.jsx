import React from 'react'
import PropTypes from 'prop-types'
import ReactDom from 'react-dom'

const Share = ({ onClick }) => {
  const shareContainer1 = document.querySelector('.btn-share-container-1')
  const shareContainer2 = document.querySelector('.btn-share-container-2')

  const getButtonShare1 = () => (
    <button
      onClick={onClick}
      type="button"
      data-click-id='share-nk'
      id='share-nk'
      data-type="1"
    >
      ПОДІЛИСЬ РЕЗУЛЬТАТОМ
      <img src="images/ar_green.svg" alt="" />
    </button>
  )

  const getButtonShare2 = () => (
    <button
      onClick={onClick}
      type="button"
      data-click-id='share-dz'
      id='share-dz'
      data-type="2"
    >
      ПОДІЛИСЬ РЕЗУЛЬТАТОМ
      <img src="images/arr_red.svg" alt="" />
    </button>
  )

  return (
    <React.Fragment>
      { ReactDom.createPortal(getButtonShare1(), shareContainer1) }
      { ReactDom.createPortal(getButtonShare2(), shareContainer2) }
    </React.Fragment>
  )
}


Share.propTypes = {
  onClick: PropTypes.func.isRequired,
}


export default Share
