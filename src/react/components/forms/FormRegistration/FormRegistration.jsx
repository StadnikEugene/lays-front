import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import 'react-datepicker/dist/react-datepicker.css'

import InputPhone from 'components/inputs/InputPhone'
import InputPassword from 'components/inputs/InputPassword'
import InputEmail from 'components/inputs/InputEmail'
import InputCheckbox from 'components/inputs/InputCheckboxTerms'
import InputName from 'components/inputs/InputName'

import './style.scss'

// Component

const FormRegistration = (props) => {
  const {
    handleSubmit,
    onClickСonditions,
    nameValidate,
    phoneValidate,
    passwordValidate,
    checkboxValidate,
    emailValidate,
    onClickTerms,
  } = props

  return (
    <div className="form form-registretion">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <Field
            type="text"
            component={InputName}
            name="name"
            placeholder="ім’я"
            validate={nameValidate}
          />
        </div>
        <div className="form-group">
          <Field
            type="tel"
            component={InputPhone}
            name="phone"
            placeholder="Введiть телефон"
            validate={phoneValidate}
          />
        </div>
        <div className="form-group">
          <Field
            type="email"
            component={InputEmail}
            name="email"
            placeholder="e-mail"
            validate={emailValidate}
          />
        </div>
        <div className="form-group">
          <Field
            type="password"
            component={InputPassword}
            name="passwordFirst"
            placeholder="Пароль*"
            validate={passwordValidate}
          />
        </div>
        <div className="form-group">
          <Field
            type="password"
            component={InputPassword}
            name="passwordSecond"
            placeholder="Підтвердити пароль*"
            validate={passwordValidate}
          />
        </div>

        <div className="form-group checkbox">
          <Field
            name="checkboxTerms"
            type="checkbox"
            component={InputCheckbox}
            id="terms"
            validate={checkboxValidate}
            htmlFor="terms"
          />
          <button
            className="button-terms"
            type="button"
            onClick={onClickTerms}
          >
            я погоджуюсь з офіційними правилами та даю згоду на обробку своїх даних
          </button>
        </div>
        <div className="form-group checkbox">
          <Field
            name="checkboxTerms2"
            type="checkbox"
            component={InputCheckbox}
            id="terms2"
            validate={checkboxValidate}
            htmlFor="terms2"
          />
          <button
            className="button-terms"
            type="button"
            onClick={onClickСonditions}
          >
            я погоджуюсь з умовами використання
          </button>
        </div>
        <input className="btn btn-style-2" type="submit" value="Зареєструватися" />
      </form>
    </div>
  )
}

FormRegistration.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onClickСonditions: PropTypes.func.isRequired,
  phoneValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  nameValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  passwordValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  emailValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  checkboxValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  onClickTerms: PropTypes.func.isRequired,
}

const FormRegistrationRedux = reduxForm({ form: 'registration' })(FormRegistration)

export default FormRegistrationRedux
