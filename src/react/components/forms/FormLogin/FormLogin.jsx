import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'


import InputPhone from 'components/inputs/InputPhone'
import InputPassword from 'components/inputs/InputPassword'

// Component

const FormLogin = (props) => {
  const {
    handleSubmit,
    phoneValidate,
    passwordValidate,
    onClickRegistartion,
    onClickRecovery,
  } = props

  return (
    <div className="form">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <Field
            type="tel"
            component={InputPhone}
            name="phone"
            placeholder="Введiть телефон"
            validate={phoneValidate}
          />
        </div>
        <div className="form-group">
          <Field
            type="password"
            component={InputPassword}
            name="password"
            placeholder="Пароль*"
            validate={passwordValidate}
          />
        </div>
        <input className="btn" type="submit" value="Увiйти" />
        <div className="recovery-link">
          <button
            onClick={onClickRecovery}
            type="button"
          >
            Забули пароль?
          </button>
        </div>
        <button
          className="btn btn-style-2"
          onClick={onClickRegistartion}
          type="button"
        >
          Зареєструватися
        </button>
      </form>
    </div>
  )
}

FormLogin.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  phoneValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  passwordValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
  onClickRegistartion: PropTypes.func.isRequired,
  onClickRecovery: PropTypes.func.isRequired,
}

const FormLoginRedux = reduxForm({ form: 'login' })(FormLogin)

export default FormLoginRedux
