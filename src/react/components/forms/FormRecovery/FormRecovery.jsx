import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'

import InputEmail from 'components/inputs/InputEmail'

// Component

const FormRecovery = (props) => {
  const {
    handleSubmit,
    emailValidate,
  } = props

  return (
    <div className="form">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <Field
            type="email"
            component={InputEmail}
            name="email"
            placeholder="Введiть email"
            validate={emailValidate}
          />
        </div>
        <div className="text-center">
          <input className="btn" type="submit" value="Вiдновити" />
        </div>
      </form>
    </div>
  )
}

FormRecovery.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  emailValidate: PropTypes.arrayOf(PropTypes.func).isRequired,
}

const FormLoginRedux = reduxForm({ form: 'recovery' })(FormRecovery)

export default FormLoginRedux
