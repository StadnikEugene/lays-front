

const counter = () => {
  if (window.counter) {
    return window.counter
  }

  class Counter {
    vote1El = document.querySelectorAll('.counter-vote1 span')

    vote2El = document.querySelectorAll('.counter-vote2 span')

    get({ type }) {
      const el = (type === 1)
        ? this.vote1El
        : this.vote2El
      let string = ''

      el.forEach((item) => {
        const span = item
        string += span.innerHTML
      })

      return Number.parseInt(string, 10)
    }

    set({ number, type }) {
      const el = (type === 1)
        ? this.vote1El
        : this.vote2El

      const numberString = number.toString()
      const startPos = 6 - numberString.length

      el.forEach((item, index) => {
        const span = item

        if (startPos > index) {
          span.innerHTML = 0
          return
        }

        span.innerHTML = numberString[index - startPos]
      })
    }

    next({ type }) {
      const number = this.get({ type }) + 1
      this.set({ number, type })
    }
  }

  window.counter = new Counter()
  return window.counter
}

export default counter()
