
export default ({ token, refreshToken }) => {
  if (token) {
    document.cookie = `token=${token}`
  }
  if (refreshToken) {
    document.cookie = `refreshToken=${refreshToken}`
  }
}
