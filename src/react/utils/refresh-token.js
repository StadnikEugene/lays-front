import readCookies from 'utils/read-cookies'
import saveTokens from 'utils/save-tokens'
import clearTokens from 'utils/clear-tokens'
import { apiRefresh } from 'api'

export default () => (
  new Promise((resolve, reject) => {
    const refreshToken = readCookies('refreshToken')

    if (!refreshToken) {
      reject()
      return
    }

    apiRefresh({ refreshToken })
      .then((token) => {
        saveTokens({ token })
        resolve()
      })
      .catch(() => {
        clearTokens()
        reject()
      })
  })
)
