import clearTokens from 'utils/clear-tokens'

export default ({ setUserAction }) => {
  setUserAction(false)
  clearTokens()
}
