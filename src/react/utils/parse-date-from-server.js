
export default date => (
  date.slice(0, 10).replace(/-/g, '.')
)
