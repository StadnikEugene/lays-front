import readCookies from 'utils/read-cookies'
import refreshToken from 'utils/refresh-token'

export default ({ api, data }) => (
  new Promise((resolve, reject) => {
    const getCodes = () => {
      const token = readCookies('token')

      if (!token) {
        reject({ message: 'access denied' })
        return
      }

      api({ token, data })
        .then((codes) => {
          resolve(codes)
        })
        .catch(({ message }) => {
          if (message === 'invalid token') {
            reject({ message: 'access denied' })
            return
          }

          if (message !== 'access token expired') {
            reject({ message })
            return
          }

          refreshToken()
            .then(() => {
              setTimeout(() => {
                getCodes()
              }, 100)
            })
            .catch(() => {
              reject({ message: 'access denied' })
            })
        })
    }
    getCodes()
  })
)
