
export default {
  disable: () => {
    window.stopScroll = true
  },
  enable: () => {
    window.stopScroll = false
  },
}
