import getCurrentPage from 'utils/get-current-page'
import getCoord from 'utils/get-coord'

export default ({ page }) => {
  const currentPageName = getCurrentPage()

  if (!currentPageName) {
    let el = null
    if (page === 1) el = document.querySelector('.first-screen')
    if (page === 2) el = document.querySelector('.second-screen')
    if (page === 3) el = document.querySelector('.third-screen')
    const { top } = getCoord(el)
    window.scrollTo({ top, behavior: 'smooth' })
    return
  }

  window.moveTo(page)
}
