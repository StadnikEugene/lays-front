
export default (cookieName) => {
  const v = document.cookie.match('(^|;) ?' + cookieName + '=([^;]*)(;|$)')
  return v ? v[2] : null
}
