import isScrollAllowed from 'utils/is-scroll-allowed'

export default () => {
  if (!isScrollAllowed()) {
    return false
  }

  return window.location.pathname.slice(1)
}
