
export default () => {
  if (!window.isScrollAllowed) {
    return false
  }

  return window.isScrollAllowed.m.up
}
