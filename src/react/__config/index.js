

const BUILD_MOD = 'prod'

export const BASE_API_PATH = (BUILD_MOD === 'prod')
  ? 'https://lays.ua/api/v1'
  : 'https://laysvoting-api.ddaproduction.com/api/v1'

export const FACEBOOK_APP_ID = '506414823485842'

export const SHARE_VOTE_1_URL = (BUILD_MOD === 'prod')
  ? 'https://lays.ua/nk'
  : 'https://laysvoting-api.ddaproduction.com/NK.html'

export const SHARE_VOTE_2_URL = (BUILD_MOD === 'prod')
  ? 'https://lays.ua/dz'
  : 'https://laysvoting-api.ddaproduction.com/DZ.html'
