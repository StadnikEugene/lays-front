import { SET_MESSAGE } from '../constants'

const defaultState = {
  message: {},
}

export const messageReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_MESSAGE:
      return { ...state, message: action.payLoad }
    default:
      return state
  }
}
