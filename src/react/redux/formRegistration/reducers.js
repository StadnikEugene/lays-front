import { SET_REGISTRATION_DATA_SEND_HENDLER, SET_REGISTRATION_FIELDS } from '../constants'

const defaultState = {
  sendDataHendler: () => {},
  fields: {},
}

export const formRegistrationReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_REGISTRATION_DATA_SEND_HENDLER:
      return { ...state, sendDataHendler: action.payLoad }
    case SET_REGISTRATION_FIELDS:
      return { ...state, fields: action.payLoad }
    default:
      return state
  }
}
