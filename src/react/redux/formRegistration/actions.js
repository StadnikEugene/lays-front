import { SET_REGISTRATION_DATA_SEND_HENDLER, SET_REGISTRATION_FIELDS } from '../constants'

export const setDataSendHendler = hendler => (
  {
    type: SET_REGISTRATION_DATA_SEND_HENDLER,
    payLoad: hendler,
  }
)

export const setRegistrationFields = fields => (
  {
    type: SET_REGISTRATION_FIELDS,
    payLoad: fields,
  }
)
