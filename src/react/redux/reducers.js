import { combineReducers } from 'redux'
import appReducers from 'redux/app/reducers'
import { formLoginReducer } from 'redux/formLogin/reducers'
import { formRegistrationReducer } from 'redux/formRegistration/reducers'
import { formRecoveryReducer } from 'redux/formRecovery/reducers'
import modalReducer from 'redux/modal/reducers'
import { shareReducer } from 'redux/share/reducers'
import { messageReducer } from 'redux/message/reducers'
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
  app: appReducers,
  formLogin: formLoginReducer,
  formRegistration: formRegistrationReducer,
  formRecovery: formRecoveryReducer,
  share: shareReducer,
  modal: modalReducer,
  message: messageReducer,
  form: formReducer,
})
