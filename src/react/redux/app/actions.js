import { USER_INFO } from '../constants'

const setUserInfo = userInfo => (
  {
    type: USER_INFO,
    payLoad: userInfo,
  }
)

export default setUserInfo
