import { USER_INFO } from '../constants'

const defaultState = {
  userInfo: null,
}

const appReducer = (state = defaultState, action) => {
  switch (action.type) {
    case USER_INFO:
      return { ...state, userInfo: action.payLoad }
    default:
      return state
  }
}

export default appReducer
