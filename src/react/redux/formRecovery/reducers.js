import { SET_RECOVERY_DATA_SEND_HENDLER } from '../constants'

const defaultState = {
  sendDataHendler: () => {},
}

export const formRecoveryReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_RECOVERY_DATA_SEND_HENDLER:
      return { ...state, sendDataHendler: action.payLoad }
    default:
      return state
  }
}
