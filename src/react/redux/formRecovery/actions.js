import { SET_RECOVERY_DATA_SEND_HENDLER } from '../constants'

const setDataSendHendler = hendler => (
  {
    type: SET_RECOVERY_DATA_SEND_HENDLER,
    payLoad: hendler,
  }
)

export default setDataSendHendler
