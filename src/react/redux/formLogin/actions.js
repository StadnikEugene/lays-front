import { SET_LOGIN_DATA_SEND_HENDLER } from '../constants'

const setDataSendHendler = hendler => (
  {
    type: SET_LOGIN_DATA_SEND_HENDLER,
    payLoad: hendler,
  }
)

export default setDataSendHendler
