import { SET_LOGIN_DATA_SEND_HENDLER } from '../constants'

const defaultState = {
  sendDataHendler: () => {},
}

export const formLoginReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_LOGIN_DATA_SEND_HENDLER:
      return { ...state, sendDataHendler: action.payLoad }
    default:
      return state
  }
}
