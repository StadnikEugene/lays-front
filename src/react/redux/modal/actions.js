import { CHANGE_MODAL, SET_CLOSE_MODAL_HENDLER } from '../constants'

export const changeModal = toggle => (
  {
    type: CHANGE_MODAL,
    payLoad: toggle,
  }
)

export const setCloseModalHendler = hendler => (
  {
    type: SET_CLOSE_MODAL_HENDLER,
    payLoad: hendler,
  }
)
