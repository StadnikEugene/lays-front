import { CHANGE_MODAL, SET_CLOSE_MODAL_HENDLER } from '../constants'

const defaultState = {
  modalName: false,
  modalMessage: false,
  modalCloseRedirect: false,
  closeModalHendler: () => {},
}

const modalReducer = (state = defaultState, action) => {
  switch (action.type) {
    case CHANGE_MODAL: {
      const { modalName, modalMessage, modalCloseRedirect } = action.payLoad
      return {
        ...state,
        modalName,
        modalMessage,
        modalCloseRedirect,
      }
    }
    case SET_CLOSE_MODAL_HENDLER:
      return { ...state, closeModalHendler: action.payLoad }
    default:
      return state
  }
}

export default modalReducer
