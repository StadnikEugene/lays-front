import { SET_SHARE_CLICK_HENDLER } from '../constants'

export const setShareClickHendler = hendler => (
  {
    type: SET_SHARE_CLICK_HENDLER,
    payLoad: hendler,
  }
)
