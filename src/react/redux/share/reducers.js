import { SET_SHARE_CLICK_HENDLER } from '../constants'

const defaultState = {
  clickHendler: () => {},
}

export const shareReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_SHARE_CLICK_HENDLER:
      return { ...state, clickHendler: action.payLoad }
    default:
      return state
  }
}
