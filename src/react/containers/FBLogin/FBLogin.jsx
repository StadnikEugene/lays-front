import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { changeModal } from 'redux/modal/actions'
import { FACEBOOK_APP_ID } from '__config'
import logout from 'utils/logout'
import setUserInfo from 'redux/app/actions'
import sendFacebookData from './send-fecebook-data'

import './style.scss'

// Container

class FBLogin extends React.Component {
  state = {}

  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    setUserInfo: PropTypes.func.isRequired,
    userInfo: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]).isRequired,
  }

  onFBResponse = ({ name, userID, picture }) => {
    const {
      userInfo,
      changeModal: changeModalAction,
      setUserInfo: setUserInfoAction,
    } = this.props

    sendFacebookData({ name, userID, picture: picture.data.url })
      .then(({ message }) => {
        if (message === 'success') {
          setUserInfoAction({ ...userInfo, userID })
          changeModalAction({ modalName: 'share' })
        }
      })
      .catch(({ message }) => {
        if (message === 'access denied') {
          logout({ setUserAction: setUserInfoAction })
          changeModalAction({ modalName: 'login' })
        }
      })
  }

  render() {
    return (
      <div className="facebook-login">
        <FacebookLogin
          appId={FACEBOOK_APP_ID}
          fields="name,email,picture"
          render={renderProps => (
            <button
              onClick={renderProps.onClick}
              className="btn btn-style-2 btn-fb-login"
              type="button"
            >
              Увійти через Facebook
            </button>
          )}
          callback={this.onFBResponse}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.app.userInfo,
})

const mapDispatchToProps = {
  setUserInfo,
  changeModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(FBLogin)
