import { apiSendFacebookData } from 'api'
import autorizedRequest from 'utils/autorized-request'

export default data => (
  autorizedRequest({ api: apiSendFacebookData, data })
)
