import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { stopSubmit, getFormValues, initialize } from 'redux-form'

import Component from 'components/forms/FormRegistration'
import {
  passwordValidate,
  phoneValidate,
  emailValidate,
  nameValidate,
  dateValidate,
  checkboxValidate,
} from '_common/form-validation'
import parseInputPhone from 'utils/parse-input-phone'
import { changeModal } from 'redux/modal/actions'
import { setRegistrationFields } from 'redux/formRegistration/actions'
import isObjectEmpty from 'utils/is-object-empty'
import messages from '_common/form-validation/messages'


// container

class FormRegistration extends React.Component {
  state = {

  }

  static propTypes = {
    sendDataHendler: PropTypes.func.isRequired,
    changeModal: PropTypes.func.isRequired,
    stopSubmit: PropTypes.func.isRequired,
    setRegistrationFields: PropTypes.func.isRequired,
    values: PropTypes.objectOf(PropTypes.any),
    fieldsRegistration: PropTypes.objectOf(PropTypes.any).isRequired,
    initialize: PropTypes.func.isRequired,
  }

  static defaultProps = {
    values: {},
  }

  componentDidMount() {
    const { fieldsRegistration: fields, initialize: initializeAction } = this.props
    if (isObjectEmpty(fields)) {
      return
    }
    initializeAction('registration', fields)
  }

  onSubmit = (formData) => {
    const {
      sendDataHendler,
      stopSubmit: stopSubmitAction,
      setRegistrationFields: setFieldsAction,
    } = this.props
    const { passwordFirst, passwordSecond } = formData

    if (passwordFirst !== passwordSecond) {
      stopSubmitAction('registration', { passwordFirst: messages.passwordsNotEqual })
      return
    }
    setFieldsAction({})
    sendDataHendler({
      ...formData,
      phone: parseInputPhone(formData.phone),
    })
  }

  onClickLogin = () => {
    const { changeModal: changeModalAction } = this.props
    changeModalAction({ modalName: 'login' })
  }


  onClickСonditions = () => {
    window.open('https://drive.google.com/open?id=1XJX-2YB4W2Y99Pxy0w3HHJ5qglUyJfUR','_blank')
  }

  onClickTerms = () => {
    const {
      changeModal: changeModalAction,
      setRegistrationFields: setFieldsAction,
      values,
    } = this.props

    setFieldsAction(values)

    changeModalAction({
      modalName: 'rules',
      modalCloseRedirect: 'registration',
    })
  }

  render() {
    return (
      <Component
        onSubmit={this.onSubmit}
        onClickСonditions={this.onClickСonditions}
        phoneValidate={phoneValidate}
        passwordValidate={passwordValidate}
        emailValidate={emailValidate}
        nameValidate={nameValidate}
        dateValidate={dateValidate}
        checkboxValidate={checkboxValidate}
        onClickLogin={this.onClickLogin}
        onClickTerms={this.onClickTerms}
      />
    )
  }
}

const mapStateToProps = state => ({
  sendDataHendler: state.formRegistration.sendDataHendler,
  values: getFormValues('registration')(state),
  fieldsRegistration: state.formRegistration.fields,
})

const mapDispatchToProps = {
  changeModal,
  stopSubmit,
  setRegistrationFields,
  initialize,
}

export default connect(mapStateToProps, mapDispatchToProps)(FormRegistration)
