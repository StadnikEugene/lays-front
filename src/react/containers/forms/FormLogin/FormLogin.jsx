import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Component from 'components/forms/FormLogin'
import { passwordValidate, phoneValidate } from '_common/form-validation'
import parseInputPhone from 'utils/parse-input-phone'
import { changeModal } from 'redux/modal/actions'


// container

class FormLogin extends React.Component {
  static propTypes = {
    sendDataHendler: PropTypes.func.isRequired,
    changeModal: PropTypes.func.isRequired,
  }

  onSubmit = (formData) => {
    const { sendDataHendler } = this.props
    sendDataHendler({
      ...formData,
      phone: parseInputPhone(formData.phone),
    })
  }

  onClickRegistartion = () => {
    const { changeModal: changeModalAction } = this.props
    changeModalAction({ modalName: 'registration' })
  }

  onClickRecovery = () => {
    const { changeModal: changeModalAction } = this.props
    changeModalAction({ modalName: 'recovery', modalCloseRedirect: 'login' })
  }

  render() {
    return (
      <Component
        onSubmit={this.onSubmit}
        phoneValidate={phoneValidate}
        passwordValidate={passwordValidate}
        onClickRegistartion={this.onClickRegistartion}
        onClickRecovery={this.onClickRecovery}
      />
    )
  }
}

const mapStateToProps = state => ({
  sendDataHendler: state.formLogin.sendDataHendler,
})

const mapDispatchToProps = {
  changeModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin)
