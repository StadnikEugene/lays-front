import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Component from 'components/forms/FormRecovery'
import { emailValidate } from '_common/form-validation'

// container

class FormRecovery extends React.Component {
  static propTypes = {
    sendDataHendler: PropTypes.func.isRequired,
  }

  onSubmit = ({ email }) => {
    const { sendDataHendler } = this.props
    sendDataHendler(email)
  }

  render() {
    return (
      <Component
        onSubmit={this.onSubmit}
        emailValidate={emailValidate}
      />
    )
  }
}

const mapStateToProps = state => ({
  sendDataHendler: state.formRecovery.sendDataHendler,
})

export default connect(mapStateToProps)(FormRecovery)
