import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { changeModal } from 'redux/modal/actions'
import Component from 'components/modals/PasswordRecovery'
import setDataSendHendler from 'redux/formRecovery/actions'
import { apiPasswordRecovery } from 'api'

// Container

class PasswordRecovery extends React.Component {
  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    setDataSendHendler: PropTypes.func.isRequired,
    closeModalHendler: PropTypes.func.isRequired,
    modalCloseRedirect: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
    ]),
  }

  static defaultProps = {
    modalCloseRedirect: false,
  }

  componentDidMount() {
    const { setDataSendHendler: setHendler } = this.props
    setHendler(this.recovery)
  }

  recovery = (email) => {
    const {
      changeModal: changeModalAction,
      modalCloseRedirect: modalRedirect,
    } = this.props

    apiPasswordRecovery(email)
      .then(() => {
        changeModalAction({ modalName: 'recoverySuccess', modalCloseRedirect: modalRedirect })
      })
      .catch(({ message }) => {
        if (!message) {
          return
        }
        changeModalAction({ modalName: 'recoveryDenied', modalCloseRedirect: 'recovery' })
      })
  }

  render() {
    const { closeModalHendler } = this.props

    return (
      <Component
        closeModal={closeModalHendler}
      />
    )
  }
}

const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
  modalCloseRedirect: state.modal.modalCloseRedirect,
})

const mapDispatchToProps = {
  setDataSendHendler,
  changeModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordRecovery)
