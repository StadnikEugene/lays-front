import { apiCheckWin } from 'api'
import autorizedRequest from 'utils/autorized-request'

export default data => (
  autorizedRequest({ api: apiCheckWin, data })
)
