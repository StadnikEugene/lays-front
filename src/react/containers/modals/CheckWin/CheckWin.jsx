import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import ComponentWin from 'components/modals/CheckWin'
import ComponentNotWin from 'components/modals/_messages/NotWin'
import { changeModal } from 'redux/modal/actions'
import setUserInfo from 'redux/app/actions'
import logout from 'utils/logout'
import sendCheckWin from './send-check-win'
import './style.scss'


// Component

class CheckWin extends React.Component {
  state = {
    win: null,
  }

  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    setUserInfo: PropTypes.func.isRequired,
    closeModalHendler: PropTypes.func.isRequired,
  }

  static propTypes = {
    closeModalHendler: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.getWin()
  }

  getWin() {
    const {
      setUserInfo: setUserInfoAction,
      changeModal: changeModalAction,
    } = this.props

    sendCheckWin()
      .then((data) => {
        this.setState({ win: data })
      })
      .catch(({ message }) => {
        if (message === 'access denied') {
          logout({ setUserAction: setUserInfoAction })
          changeModalAction({ modalName: 'login' })
        }
      })
  }

  render() {
    const { closeModalHendler } = this.props
    const { win } = this.state

    if (!win) {
      return false
    }

    const isWin = win.length !== 0

    return (
      <React.Fragment>
        {isWin && <ComponentWin win={win} closeModalHendler={closeModalHendler} />}
        {!isWin && <ComponentNotWin closeModalHendler={closeModalHendler} />}
      </React.Fragment>
    )
  }
}


const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
  changeModal,
  setUserInfo,
})

export default connect(mapStateToProps)(CheckWin)
