import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { stopSubmit } from 'redux-form'

import Component from 'components/modals/Registration'
import setUserInfo from 'redux/app/actions'
import { apiRegistration } from 'api'
import { setDataSendHendler, setRegistrationFields } from 'redux/formRegistration/actions'
import saveTokens from 'utils/save-tokens'

// Container

class Registration extends React.Component {
  static propTypes = {
    setDataSendHendler: PropTypes.func.isRequired,
    stopSubmit: PropTypes.func.isRequired,
    setUserInfo: PropTypes.func.isRequired,
    closeModalHendler: PropTypes.func.isRequired,
    setRegistrationFields: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const {
      setDataSendHendler: setDataHendler,
    } = this.props
    setDataHendler(this.sendRegistrationData)
  }

  onCloseModal = () => {
    const {
      closeModalHendler,
      setRegistrationFields: setFieldsAction,
    } = this.props
    setFieldsAction({})
    closeModalHendler()
  }

  sendRegistrationData = (formDate) => {
    const {
      stopSubmit: stopSubmitAction,
      setUserInfo: setUserInfoAction,
      closeModalHendler: closeModalAction,
    } = this.props

    const {
      name,
      phone,
      date: dob,
      email,
      passwordFirst: password,
    } = formDate

    if (this.isSubmit) {
      return
    }

    this.isSubmit = true

    apiRegistration({
      name,
      phone,
      dob,
      email,
      password,
    })
      .then(({ userData, token, refreshToken }) => {
        this.isSubmit = false
        setUserInfoAction(userData)
        saveTokens({ token, refreshToken })
        closeModalAction()
      })
      .catch(({ code, message }) => {
        this.isSubmit = false
        if (!code) {
          return
        }
        stopSubmitAction('registration', { [code]: message })
      })
  }

  render() {
    return (
      <Component
        closeModal={this.onCloseModal}
      />
    )
  }
}

const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
})

const mapDispatchToProps = {
  setRegistrationFields,
  setDataSendHendler,
  stopSubmit,
  setUserInfo,
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration)
