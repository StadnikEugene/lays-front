import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Component from 'components/modals/Rules'
import { apiGetRules } from 'api'
import { changeModal } from 'redux/modal/actions'

// Container

class Rules extends React.Component {
  state = {
    rules: false,
  }

  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    closeModalHendler: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.getRules()
  }

  getRules = () => {
    const { changeModal: changeModalAction } = this.props

    apiGetRules()
      .then((rules) => {
        this.setState({ rules })
      })
      .catch(() => {
        changeModalAction({ modalName: false })
      })
  }

  render() {
    const { closeModalHendler } = this.props
    const { rules } = this.state

    if (!rules) {
      return false
    }

    return (
      <Component
        closeModal={closeModalHendler}
        rules={rules}
      />
    )
  }
}

const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
})

const mapDispatchToProps = {
  changeModal,
}


export default connect(mapStateToProps, mapDispatchToProps)(Rules)
