import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { apiGetWinners } from 'api'
import Component from 'components/modals/Winners'
import './style.scss'


// Component

class Winners extends React.Component {
  state = {
    winners: null,
  }

  static propTypes = {
    closeModalHendler: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.getWinners()

  }

  getWinners() {
    apiGetWinners()
      .then((winners) => {
        this.setState({ winners })
      })
  }

  render() {
    const { closeModalHendler } = this.props
    const { winners } = this.state

    if (!winners) {
      return false
    }

    return (
      <Component
        winners={winners}
        closeModalHendler={closeModalHendler}
      />
    )
  }
}

const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
})

export default connect(mapStateToProps)(Winners)
