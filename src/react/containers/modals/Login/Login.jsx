import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { stopSubmit } from 'redux-form'

import Component from 'components/modals/Login'
import setUserInfo from 'redux/app/actions'
import { apiLogin } from 'api'
import setDataSendHendler from 'redux/formLogin/actions'
import saveTokens from 'utils/save-tokens'

// Container

class Login extends React.Component {
  state = {}

  static propTypes = {
    setDataSendHendler: PropTypes.func.isRequired,
    stopSubmit: PropTypes.func.isRequired,
    setUserInfo: PropTypes.func.isRequired,
    closeModalHendler: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { setDataSendHendler: setHendler } = this.props
    setHendler(this.sendLoginData)
  }

  sendLoginData = (data) => {
    const {
      stopSubmit: stopSubmitAction,
      setUserInfo: setUserInfoAction,
      closeModalHendler: closeModalAction,
    } = this.props

    if (this.isSubmit) {
      return
    }

    this.isSubmit = true

    apiLogin(data)
      .then(({ userData, token, refreshToken }) => {
        this.isSubmit = false
        setUserInfoAction(userData)
        saveTokens({ token, refreshToken })
        closeModalAction()
      })
      .catch((status) => {
        this.isSubmit = false
        if (status === 'failure') {
          stopSubmitAction('login', { phone: 'не дійсний номер телефону або пароль' })
        }
      })
  }

  render() {
    const { closeModalHendler } = this.props

    return (
      <Component
        closeModal={closeModalHendler}
      />
    )
  }
}

const mapStateToProps = state => ({
  closeModalHendler: state.modal.closeModalHendler,
})

const mapDispatchToProps = {
  setDataSendHendler,
  stopSubmit,
  setUserInfo,
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
