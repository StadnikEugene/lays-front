import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { changeModal } from 'redux/modal/actions'
import { setShareClickHendler } from 'redux/share/actions'
import Component from 'components/Share'

// Container

class Share extends React.Component {
  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    setShareClickHendler: PropTypes.func.isRequired,
    clickHendler: PropTypes.func.isRequired,
    userInfo: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }

  static defaultProps = {
    userInfo: null,
  }

  componentDidMount() {
    const { setShareClickHendler: clickHendlerAction } = this.props
    clickHendlerAction(this.onClick)
  }

  onClick = (e) => {
    const {
      userInfo,
      changeModal: changeModalAction,
    } = this.props

    if (!userInfo) {
      changeModalAction({ modalName: 'login' })
      return
    }

    if (userInfo.share) {
      changeModalAction({ modalName: 'shareDenied' })
      return
    }

    if (!userInfo.vote) {
      changeModalAction({ modalName: 'shareNotVoted' })
      return
    }

    const voteType = e.target.dataset.type
      ? e.target.dataset.type
      : userInfo.vote.toString()

    if (userInfo.vote.toString() !== voteType) {
      changeModalAction({ modalName: 'shareNotMine' })
      return
    }

    if (!userInfo.userID) {
      changeModalAction({ modalName: 'facebookLogin' })
      return
    }

    changeModalAction({ modalName: 'share' })
  }

  render() {
    const { clickHendler } = this.props

    return (
      <Component onClick={clickHendler} />
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.app.userInfo,
  clickHendler: state.share.clickHendler,
})

const mapDispatchToProps = {
  changeModal,
  setShareClickHendler,
}

export default connect(mapStateToProps, mapDispatchToProps)(Share)
