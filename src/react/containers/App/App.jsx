import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'


import VotesButtons from 'containers/Votes'
import ShareButtons from 'containers/Share'
import CheckWin from 'containers/modals/CheckWin'
import RecoverySuccess from 'components/modals/_messages/RecoverySuccess'
import FacebookLogin from 'components/modals/FacebookLogin'
import ShareNotVoted from 'components/modals/_messages/ShareNotVoted'
import ShareNotMine from 'components/modals/_messages/ShareNotMine'
import RecoveryDenied from 'components/modals/_messages/RecoveryDenied'
import VoteDenied from 'components/modals/_messages/VoteDenied'
import VoteSuccess from 'components/modals/_messages/VoteSuccess'
import NotWin from 'components/modals/_messages/NotWin'
import ShareDenied from 'components/modals/_messages/ShareDenied'
import ShareSuccess from 'components/modals/_messages/ShareSuccess'
import ShareAfter from 'components/modals/_messages/ShareAfter'
import Share from 'components/modals/Share'
import Winners from 'containers/modals/Winners'
import Login from 'containers/modals/Login'
import Rules from 'containers/modals/Rules'
import PasswordRecovery from 'containers/modals/PasswordRecovery'
import Registration from 'containers/modals/Registration'
import Message from '_common/components/Message'
import setUserInfo from 'redux/app/actions'
import { changeModal } from 'redux/modal/actions'
import logout from 'utils/logout'
import counter from 'utils/counter'
import { apiGetWeek, apiGetVotes } from 'api'
import setClickHendlers from './set-click-hendlers'
import visualization from './visualization'
import isAutorized from './is-autorized'
import setWeekHendler from './set-week-hendler'


class App extends React.Component {
  static propTypes = {
    setUserInfo: PropTypes.func.isRequired,
    changeModal: PropTypes.func.isRequired,
    clickSlideShareHendler: PropTypes.func.isRequired,
    modal: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]).isRequired,
    userInfo: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }

  static defaultProps = {
    userInfo: null,
  }

  componentDidMount() {
    const {
      setUserInfo: setUserAction,
    } = this.props


    isAutorized()
      .then((result) => {
        const userInfo = (result.data) ? result.data.data : false
        visualization({ userInfo })

        if (!userInfo) {
          logout({ setUserAction })
          return
        }

        setUserAction(userInfo)
      })

    apiGetWeek()
      .then((week) => {
        setWeekHendler({ week: Number(week) - 1 })
      })

    apiGetVotes()
      .then((votes) => {
        counter.set({ number: votes[0], type: 1 })
        counter.set({ number: votes[1], type: 2 })
      })

    if (window.location.pathname === '/') {
      window.location.pathname = '/main'
    }
  }

  componentDidUpdate(prevProps) {
    const { clickSlideShareHendler: lastClickSlideHendler } = prevProps
    const {
      clickSlideShareHendler: clickSlideHendler,
      changeModal: changeModalAction,
      setUserInfo: setUserAction,
    } = this.props

    if (lastClickSlideHendler !== clickSlideHendler) {
      setClickHendlers({ changeModalAction, setUserAction, clickSlideHendler })
    }

    const { userInfo, modal } = this.props
    visualization({ userInfo, modal })
  }

  render() {
    const { userInfo, modal } = this.props

    if (userInfo === null) {
      return false
    }

    return (
      <React.Fragment>
        <VotesButtons />
        <ShareButtons />
        { modal === 'login' && <Login /> }
        { modal === 'checkWin' && <CheckWin /> }
        { modal === 'registration' && <Registration /> }
        { modal === 'message' && <Message /> }
        { modal === 'rules' && <Rules /> }
        { modal === 'recovery' && <PasswordRecovery /> }
        { modal === 'winners' && <Winners /> }
        { modal === 'recoverySuccess' && <RecoverySuccess /> }
        { modal === 'recoveryDenied' && <RecoveryDenied /> }
        { modal === 'voteDenied' && <VoteDenied /> }
        { modal === 'voteSuccess' && <VoteSuccess /> }
        { modal === 'shareSuccess' && <ShareSuccess /> }
        { modal === 'shareAfter' && <ShareAfter /> }
        { modal === 'shareDenied' && <ShareDenied /> }
        { modal === 'shareNotVoted' && <ShareNotVoted /> }
        { modal === 'shareNotMine' && <ShareNotMine /> }
        { modal === 'facebookLogin' && <FacebookLogin /> }
        { modal === 'notWin' && <NotWin /> }
        { modal === 'share' && <Share /> }
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.app.userInfo,
  modal: state.modal.modalName,
  clickSlideShareHendler: state.share.clickHendler,
})

const mapDispatchToProps = {
  setUserInfo,
  changeModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
