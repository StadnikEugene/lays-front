import logout from 'utils/logout'
import goToPage from 'utils/go-to-page'

export default ({ changeModalAction, setUserAction, clickSlideHendler }) => {
  const loginButton = document.querySelector('.menu__content .login')
  const logoutButton = document.querySelector('.menu__content .logout')
  const winnersButton = document.querySelector('.menu__content .winners')
  const starsButton = document.querySelector('.menu__content .stars')
  const gadgetButton = document.querySelector('.menu__content .gadget')
  const winButton = document.querySelector('.menu__content .win')
  const faqButton = document.querySelector('.menu__content .faq')

  const voteLink1 = document.querySelector('.m-h__btn .vote-1')
  const voteLink2 = document.querySelector('.m-h__btn .vote-2')
  const shareLink1 = document.querySelector('.pack-l button.share')
  const shareLink2 = document.querySelector('.pack-r button.share')

  const slideGadget = document.querySelector('.stages-box .slide-gadget')
  const slideShare = document.querySelector('.stages-box .slide-share')
  const slideMonth = document.querySelector('.stages-box .slide-month')


  const menu = document.querySelector('.menu')
  const menuToggler = document.querySelector('.menu__toggler')


  const closeMenu = () => {
    menu.classList.remove('opened')
    menuToggler.classList.remove('opened')
  }

  winnersButton.addEventListener('click', () => {
    changeModalAction({ modalName: 'winners' })
    closeMenu()
  })
  faqButton.addEventListener('click', () => {
    changeModalAction({ modalName: 'rules' })
    closeMenu()
  })
  winButton.addEventListener('click', () => {
    changeModalAction({ modalName: 'checkWin' })
    closeMenu()
  })
  loginButton.addEventListener('click', () => {
    changeModalAction({ modalName: 'login' })
    closeMenu()
  })

  logoutButton.addEventListener('click', () => {
    logout({ setUserAction })
    closeMenu()
  })
  starsButton.addEventListener('click', () => {
    goToPage({ page: 3 })
    closeMenu()
  })
  gadgetButton.addEventListener('click', () => {
    goToPage({ page: 2 })
    closeMenu()
  })


  voteLink1.addEventListener('click', () => {
    goToPage({ page: 1 })
  })
  voteLink2.addEventListener('click', () => {
    goToPage({ page: 1 })
  })
  shareLink1.addEventListener('click', () => {
    goToPage({ page: 3 })
  })
  shareLink2.addEventListener('click', () => {
    goToPage({ page: 3 })
  })


  slideGadget.addEventListener('click', () => {
    goToPage({ page: 2 })
  })
  slideMonth.addEventListener('click', () => {
    goToPage({ page: 3 })
  })

  slideShare.addEventListener('click', clickSlideHendler)
}
