
export default ({ week }) => {
  const gadget = document.querySelector('.second-screen .gadget')

  const setGadget = (nextSlide) => {
    if (week - 1 === nextSlide) {
      gadget.classList.add('active')
    } else {
      gadget.classList.remove('active')
    }
  }

  gadget.classList.add('active')

  window.$('.prizes-carousel').on('beforeChange', (event, slick, currentSlide, nextSlide) => {
    setGadget(nextSlide)
  })
}
