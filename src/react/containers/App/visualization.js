
export default ({ userInfo, modal }) => {
  const loginButton = document.querySelector('.menu__content .login')
  const logoutButton = document.querySelector('.menu__content .logout')
  const winButton = document.querySelector('.menu__content .win')
  const voteLink1 = document.querySelector('.m-h__btn .vote-1 span')
  const voteLink2 = document.querySelector('.m-h__btn .vote-2 span')

  if (!userInfo) {
    loginButton.classList.remove('hidden')
    logoutButton.classList.add('hidden')
    winButton.classList.add('hidden')
  } else {
    loginButton.classList.add('hidden')
    logoutButton.classList.remove('hidden')
    winButton.classList.remove('hidden')
  }

  if (modal) {
    document.body.classList.add('modal-open')
  } else {
    document.body.classList.remove('modal-open')
  }

  const { vote } = userInfo

  if (vote === 1) {
    voteLink1.innerHTML = 'ОБРАНИЙ ТОБОЮ СМАК'
  } else {
    voteLink1.innerHTML = 'голосуй за смак'
  }

  if (vote === 2) {
    voteLink2.innerHTML = 'ОБРАНИЙ ТОБОЮ СМАК'
  } else {
    voteLink2.innerHTML = 'голосуй за смак'
  }
}
