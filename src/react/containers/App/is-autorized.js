import { apiUserInfo } from 'api'
import autorizedRequest from 'utils/autorized-request'

export default () => (
  new Promise((resolve) => {
    autorizedRequest({ api: apiUserInfo })
      .then((userInfo) => {
        resolve(userInfo)
      })
      .catch(() => {
        resolve(false)
      })
  })
)
