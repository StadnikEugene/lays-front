import { apiSendFacebookShare } from 'api'
import autorizedRequest from 'utils/autorized-request'

export default data => (
  autorizedRequest({ api: apiSendFacebookShare, data })
)
