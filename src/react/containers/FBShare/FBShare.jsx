import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FacebookShareButton } from 'react-share'
import { changeModal } from 'redux/modal/actions'
import setUserInfo from 'redux/app/actions'
import logout from 'utils/logout'
import { SHARE_VOTE_1_URL, SHARE_VOTE_2_URL } from '__config'
import sendFacebookShare from './send-fecebook-share'
import './style.scss'

// Container

class FBShare extends React.Component {
  state = {}

  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    setUserInfo: PropTypes.func.isRequired,
    userInfo: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]).isRequired,
  }

  sendShare = () => {
    const {
      userInfo,
      setUserInfo: setUserInfoAction,
      changeModal: changeModalAction,
    } = this.props
    sendFacebookShare({ share: userInfo.vote })
      .then(({ message }) => {
        if (message === 'shared') {
          setUserInfoAction({ ...userInfo, share: userInfo.vote })
          changeModalAction({ modalName: 'shareSuccess' })
        }
      })
      .catch(({ message }) => {
        if (message === 'access denied') {
          logout({ setUserAction: setUserInfoAction })
          changeModalAction({ modalName: 'login' })
        }
        if (message === 'already shared') {
          changeModalAction({ modalName: 'shareDenied' })
        }
      })
  }

  render() {
    const { userInfo } = this.props

    const url = (parseInt(userInfo.vote, 10) === 1)
      ? SHARE_VOTE_1_URL
      : SHARE_VOTE_2_URL

    return (
      <div className="facebook-share">
        <FacebookShareButton
          onShareWindowClose={this.sendShare}
          className="btn btn-style-1 btn-fb-share"
          url={url}
        >
          Share
        </FacebookShareButton>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.app.userInfo,
})

const mapDispatchToProps = {
  setUserInfo,
  changeModal,
}

export default connect(mapStateToProps, mapDispatchToProps)(FBShare)
