import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import setUserInfo from 'redux/app/actions'
import { changeModal } from 'redux/modal/actions'
import Component from 'components/Votes'
import logout from 'utils/logout'
import counter from 'utils/counter'
import sendVote from './send-vote'
import addClickHendler from './add-click-hendler'

// Container

class Votes extends React.Component {
  static propTypes = {
    changeModal: PropTypes.func.isRequired,
    setUserInfo: PropTypes.func.isRequired,
    userInfo: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
  }

  static defaultProps = {
    userInfo: null,
  }

  componentDidMount() {
    addClickHendler()
  }

  onClick = (e) => {
    const {
      userInfo,
      setUserInfo: setUserInfoAction,
      changeModal: changeModalAction,
    } = this.props

    if (!userInfo) {
      logout({ setUserAction: setUserInfoAction })
      changeModalAction({ modalName: 'login' })
      return
    }
    if (userInfo.vote) {
      changeModalAction({ modalName: 'voteDenied' })
      return
    }

    const voteType = e.target.dataset.type

    sendVote({ type: voteType })
      .then(({ message }) => {
        if (message === 'done') {
          counter.next({ type: voteType })
          setUserInfoAction({ ...userInfo, vote: voteType })
          changeModalAction({ modalName: 'voteSuccess', modalCloseRedirect: 'shareAfter' })
        }
      })
      .catch(({ message }) => {
        if (message === 'access denied') {
          logout({ setUserAction: setUserInfoAction })
          changeModalAction({ modalName: 'login' })
          return
        }
        if (message === 'already voted') {
          changeModalAction({ modalName: 'voteDenied' })
        }
      })
  }

  render() {
    const { userInfo } = this.props

    return (
      <Component
        onClick={this.onClick}
        voted={userInfo.vote}
      />
    )
  }
}

const mapStateToProps = state => ({
  userInfo: state.app.userInfo,
})

const mapDispatchToProps = {
  changeModal,
  setUserInfo,
}

export default connect(mapStateToProps, mapDispatchToProps)(Votes)
