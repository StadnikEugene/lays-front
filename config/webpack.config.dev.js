const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

module.exports = ({
  entry,
  filename,
  publicPath,
  outputPath,
  resolvePath,
  devServerPort,
}) => (
  {
    mode: 'development',
    entry,
    output: {
      filename,
      chunkFilename: 'js/[name].js',
      publicPath,
      path: outputPath,
    },
    module: {
      rules: [
        {
          test: /\.jsx$|\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.(png|jp?g|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'images/[name].[ext]',
              },
            },
          ],
        },
        {
          test: /\.(ttf|woff|svg|eot)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'fonts/[name].[ext]',
              },
            },
          ],
        },
        {
          test: /\.scss$|\.css$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: resolvePath.map(item => (
                  path.join(__dirname, item)
                )),
              },
            },
          ],
        },
      ],
    },
    resolve: {
      modules: [...resolvePath, 'node_modules'],
      extensions: ['.js', '.jsx', '.scss'],
    },
    devServer: {
      inline: true,
      contentBase: outputPath,
      port: devServerPort,
      historyApiFallback: true,
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: 'My App',
        template: './src/index.html',
        filename: 'index.html',
      }),
      new CopyPlugin([
        { from: './src/css', to: 'css' },
        { from: './src/js', to: 'js' },
        { from: './src/fonts', to: 'fonts' },
        { from: './src/images', to: 'images' },
      ]),
    ],
    devtool: 'eval-cheap-module-source-map',
    watch: true,
  }
)
